# Dodawanie widgetu na stronę.
Do strony należy dodać css zawierający styl widgetu
`<link rel="stylesheet" type="text/css" href="https://fedox.pl/cdn/vote.min.css">` oraz js zawierający skrypt 
`<script src="https://fedox.pl/cdn/vote.min.js"></script>`
potem w miejscu gdzie chcemy żeby pojawił się widget należy dodać pusty DIV `<div class="vote-fedox"></div>`

Przykład działania jest dostępny [tutaj](http://www.pzbs.pl/wyniki/ferie/2019/test/)

Wszystkie błędy proszę zgłaszać w zakładce [issues](https://bitbucket.org/fedox8/vote-pzbs/issues/new)

## Kopiuj i wklej ten kod na stronę
### Nie wyśrodkowane
Zalecane użytkownikom, którzy mają jakieś doświadczenie z HTML i CSS
```
<div class="vote-fedox"></div>
<link rel="stylesheet" type="text/css" href="https://fedox.pl/cdn/vote.min.css">
<script src="https://fedox.pl/cdn/vote.min.js"></script>
```
![img](https://fedox.pl/i/LMOCh7uyDUsCTP3J)

### Wyśrodkowane
Zalecane użytkownikom, którzy po prostu chcą wrzucić widget do głosowania do feedu wiadomoścu
```
<div style="width:100%">
<div class="vote-fedox" style="margin:auto;"></div>
<script src="https://fedox.pl/cdn/vote.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://fedox.pl/cdn/vote.min.css">
</div>
```
![img](https://fedox.pl/i/zrraPF4d0DTj2xVW)


