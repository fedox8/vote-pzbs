/*
 * vote.js developed by Fedox & J. Mazur as part of project "Plebiscyt"
 * Last modified: 29.01.19 07:49
 * Copyright (c) 2019
 *
 * You are allowed to:
 * - edit this file
 * - use this file in your own project
 *
 * You are not allowed to:
 * - sell this file
 * - delete this copyright notice
 * - change this copyright notice
 */
const xhr = new XMLHttpRequest();
xhr.open('GET', 'https://fedox.pl/bridge/plebiscyt/api');
xhr.onload = function () {
    if (xhr.status === 200) {
        let response = JSON.parse(xhr.responseText);
        let html = `
    <div id="header-fedox">
        <div id="title-fedox">Aktualne wyniki</div>
        <div id="counter-fedox">Oddanych głosów: ${response.vote_count}</div>
        <a class="button-fedox" href="https://plebiscyt.pzbs.pl/" target="_blank">Zagłosuj!</a>
    </div>
    <div class="body-fedox">
        <div class="bodypart-fedox">
            <p> 1. ${response.ranking[0].name} - ${response.ranking[0].points}</p>
            <p> 2. ${response.ranking[1].name} - ${response.ranking[1].points}</p>
            <p> 3. ${response.ranking[2].name} - ${response.ranking[2].points}</p>
            <p> 4. ${response.ranking[3].name} - ${response.ranking[3].points}</p>
            <p> 5. ${response.ranking[4].name} - ${response.ranking[4].points}</p>
        </div>
        <div class="bodypart-fedox">
            <p> 6. ${response.ranking[5].name} - ${response.ranking[5].points}</p>
            <p> 7. ${response.ranking[6].name} - ${response.ranking[6].points}</p>
            <p> 8. ${response.ranking[7].name} - ${response.ranking[7].points}</p>
            <p> 9. ${response.ranking[8].name} - ${response.ranking[8].points}</p>
            <p> 10. ${response.ranking[9].name} - ${response.ranking[9].points}</p>
        </div>
    </div>`;
        const div = document.querySelector('.vote-fedox');
        div.innerHTML = html;
    } else {
        document.querySelector('.vote-fedox').innerHTML = `<div id="header-fedox"><div id = "title-fedox" > Błąd pobierania danych </div></div>`
    }
};
xhr.send();
